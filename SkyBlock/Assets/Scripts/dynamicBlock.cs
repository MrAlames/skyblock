﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class dynamicBlock : MonoBehaviour
{
    public Transform Player;
    [SerializeField]
    private float dist;
    public Vector3 middle;
    Vector2 cubePos2d;
    Vector2 playerPos2d;
    public float minHeight;
    public float maxHeight;
    public float minDist;
    public float maxDist;
    // Start is called before the first frame update
    void Start()
    {
        Player = GameObject.Find("Player").GetComponent<Transform>();
        middle = gameObject.GetComponent<Renderer>().bounds.center;
        cubePos2d = new Vector2(middle.x, middle.z);
        playerPos2d = new Vector2(Player.position.x, Player.position.z);
        minHeight = 0f;
        maxHeight = 20f;
        maxDist = 18f;
        minDist = 12f;
    }

    // Update is called once per frame
    void Update()
    {
        playerPos2d.x = Player.position.x;
        playerPos2d.y = Player.position.z;
        dist = Vector2.Distance(playerPos2d, cubePos2d);
        Vector3 tmp = this.transform.position;
        Vector3 tmpScale = transform.localScale;

        if (dist > maxDist)
        {
            tmp.y = minHeight;
            tmpScale.x = 0f;
            tmpScale.y = 0f;
            tmpScale.z = 0f;
        }
        else if (dist <= minDist)
        {
            tmp.y = maxHeight;
            tmpScale.x = 3f;
            tmpScale.y = 1f;
            tmpScale.z = 3f;
        }
        else
        {
            tmp.y = scale(minDist, maxDist, maxHeight, minHeight, dist);
            tmpScale.x = scale(minDist, maxDist, 3, 0, dist);
            tmpScale.y = scale(minDist, maxDist, 1, 0, dist);
            tmpScale.z = scale(minDist, maxDist, 3, 0, dist);
        }
        this.transform.position = tmp;
        this.transform.localScale = tmpScale;
    }

    public float scale(float OldMin, float OldMax, float NewMin, float NewMax, float OldValue)
    {
        return (((OldValue - OldMin) * (NewMax - NewMin)) / (OldMax - OldMin)) + NewMin;
    }
}
