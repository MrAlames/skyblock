﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Security.Cryptography;
using System.Threading;
using UnityEngine;

public class MouseLook : MonoBehaviour
{
    public float mouseSensitivity = 1000f;
    public float smoothSpeed = 0.125f;
    public Transform playerBody;
    float xRotation = 0f;
    float xOld;
    float yOld;
    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        xOld = 0f;
        yOld = 0f;
    }

    // Update is called once per frame
    void Update()
    {
        float xMove = Input.GetAxis("Mouse X") * mouseSensitivity * Time.deltaTime*smoothSpeed;
        float yMove = Input.GetAxis("Mouse Y") * mouseSensitivity * Time.deltaTime;

        xRotation -= yMove*smoothSpeed;
        xRotation = Mathf.Clamp(xRotation, -90f, 90f);
        transform.localRotation = Quaternion.Euler(xRotation, 0f, 0f);
        playerBody.Rotate(Vector3.up * xMove);
        xOld = xMove;
        yOld = yMove;
    }

    /*public Transform target;

    
    public Vector3 offset;

    void FixedUpdate()
    {
        Vector3 desiredPosition = target.position + offset;
        Vector3 smoothedPosition = Vector3.Lerp(transform.position, desiredPosition, smoothSpeed);
        transform.position = smoothedPosition;

        transform.LookAt(target);
    }
    */
}
